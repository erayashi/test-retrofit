package com.eaz.testretrofit;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.eaz.testretrofit.R;
import com.eaz.testretrofit.resource.network.MovieIface;
import com.eaz.testretrofit.resource.network.TmdbClient;
import com.eaz.testretrofit.resource.network.response.ApiResponse;
import com.eaz.testretrofit.resource.network.response.Movie;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {
    private MovieAdapter movieAdapter = new MovieAdapter();
    private ProgressBar pbLoading;

    public MainFragment() {
        // Required empty public constructor
        Log.d(MainFragment.class.getSimpleName(), "MainFragment: haloo dari fragment");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView rvMovie = view.findViewById(R.id.rv_movie);
        rvMovie.setAdapter(movieAdapter);
        pbLoading = view.findViewById(R.id.pb_movie);
        getMovies();
    }

    private void getMovies() {
        pbLoading.setVisibility(View.VISIBLE);
        MovieIface movieIface = TmdbClient.Retrofit().create(MovieIface.class);
        movieIface.getNowPlaying(1).enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call,
                                   Response<ApiResponse> response) {
                if(response.body() != null) {
                    movieAdapter.setMovies(response.body().getResults());
                } else {
                    Toast.makeText(getActivity(), "error tidak dapat load data dari internet", Toast.LENGTH_SHORT).show();
                }
                pbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse> call,
                                  Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), "error tidak dapat load data dari internet", Toast.LENGTH_SHORT).show();
                pbLoading.setVisibility(View.GONE);
            }
        });
    }
}
