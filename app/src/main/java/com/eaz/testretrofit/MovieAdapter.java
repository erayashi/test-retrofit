package com.eaz.testretrofit;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eaz.testretrofit.resource.network.TmdbClient;
import com.eaz.testretrofit.resource.network.response.Movie;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieVH> {
    private List<Movie> movies = new ArrayList<>();
    private Context context;

    @NonNull
    @Override
    public MovieVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new MovieVH(inflater.inflate(R.layout.item_movie, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MovieVH holder, int position) {
        Movie movie = movies.get(position);
        if(movie != null) {
            holder.bind(movie);
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void setMovies(List<Movie> movies) {
        this.movies.clear();
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    class MovieVH extends RecyclerView.ViewHolder {
        TextView tvTitle;
        ImageView ivPoster;

        MovieVH(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            ivPoster = itemView.findViewById(R.id.iv_poster);
        }

        void bind(Movie movie) {
            Log.d(MovieAdapter.class.getSimpleName(), "bind: " + movie.getTitle());
            tvTitle.setText(movie.getTitle());
            TmdbClient.LoadImage(context, ivPoster, movie.getPoster_path());
        }
    }
}
