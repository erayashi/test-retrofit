package com.eaz.testretrofit.resource.network;

import com.eaz.testretrofit.resource.network.response.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieIface {
    @GET("movie/popular")
    Call<ApiResponse> getNowPlaying(@Query("page") int page);
}
