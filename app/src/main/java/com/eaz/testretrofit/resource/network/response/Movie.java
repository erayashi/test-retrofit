package com.eaz.testretrofit.resource.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

public class Movie implements Parcelable {
    private int id;
    private Double vote_average;
    private String title;
    private String poster_path;
    private String overview;
    private Date release_date;
    private ArrayList<Genre> genres;
    private String staring;
    private String directed_by;
    private String genreStr;

    public Movie(){

    }

    public Movie(Double vote_average, String title, String poster_path, String overview, Date release_date, ArrayList<Genre> genres) {
        this.vote_average = vote_average;
        this.title = title;
        this.poster_path = poster_path;
        this.overview = overview;
        this.release_date = release_date;
        this.genres = genres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getVote_average() {
        return vote_average;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getOverview() {
        return overview;
    }

    public Date getRelease_date() {
        return release_date;
    }

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public String getStaring() {
        return staring;
    }

    public String getDirected_by() {
        return directed_by;
    }

    public String getGenreStr() {
        return genreStr;
    }

    public void setGenreStr(String genreStr) {
        this.genreStr = genreStr;
    }

    public class Genre {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeValue(this.vote_average);
        dest.writeString(this.title);
        dest.writeString(this.poster_path);
        dest.writeString(this.overview);
        dest.writeLong(this.release_date != null ? this.release_date.getTime() : -1);
        dest.writeString(this.staring);
        dest.writeString(this.directed_by);
        dest.writeString(this.genreStr);
    }

    protected Movie(Parcel in) {
        this.id = in.readInt();
        this.vote_average = (Double) in.readValue(Double.class.getClassLoader());
        this.title = in.readString();
        this.poster_path = in.readString();
        this.overview = in.readString();
        long tmpRelease_date = in.readLong();
        this.release_date = tmpRelease_date == -1 ? null : new Date(tmpRelease_date);
        this.staring = in.readString();
        this.directed_by = in.readString();
        this.genreStr = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
